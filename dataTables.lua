local GlobalAddonName, AIU = ...

AIU.buffs =
{
    ["Flask"] =
    {
        {"PrimStat", {307185, 307166}},
        {"Stamina", {307187}},
    },
    ["Food"] =
    {
        {"Versatility", {308514}},
        {"Haste", {308488}},
        {"Mastery", {308506}},
        {"Critical Strike", {308434}},
        {"Stamina", {327707}},
    },
    ["Rune"] =
    {
        {"Augment", {347901}}
    },
    ["RaidBuff"] =
    {
        {"Intellect", {1459}},
        {"Stamina", {21562}},
        {"Attack Power", {6673}},
    },
    ["Weapon"] =
    {
        {"Embalmer", {6190, 171286}},
        {"Shadowcore", {6188, 171285}},
        {"Weighted", {6201, 171439}},
        {"Sharpened", {6200, 171437}},
    },
}